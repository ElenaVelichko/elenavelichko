;(() => {
    function showContent(event) {

        var content = document.querySelector('#about');

        var contentCoords = content.getBoundingClientRect().top + window.pageYOffset - content.offsetHeight;

        var banner = document.querySelector('.about__banner');
        var text = document.querySelector('.about__description');

        var currentScroll = window.pageYOffset;

        if (currentScroll > contentCoords) {
            banner.classList.add('bounceInLeft');
            banner.classList.add('animated');
            text.classList.add('animated');
            text.classList.add('bounceInRight');

        }

        if (currentScroll < contentCoords) {
            banner.classList.remove('animated');
            banner.classList.remove('bounceInLeft');
            text.classList.remove('animated');
            text.classList.remove('bounceInRight');

        }
    }

    document.addEventListener('DOMContentLoaded',() => {
        document.addEventListener('scroll', showContent)

    });
})();




//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJhYm91dC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyI7KCgpID0+IHtcclxuICAgIGZ1bmN0aW9uIHNob3dDb250ZW50KGV2ZW50KSB7XHJcblxyXG4gICAgICAgIHZhciBjb250ZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2Fib3V0Jyk7XHJcblxyXG4gICAgICAgIHZhciBjb250ZW50Q29vcmRzID0gY29udGVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3AgKyB3aW5kb3cucGFnZVlPZmZzZXQgLSBjb250ZW50Lm9mZnNldEhlaWdodDtcclxuXHJcbiAgICAgICAgdmFyIGJhbm5lciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5hYm91dF9fYmFubmVyJyk7XHJcbiAgICAgICAgdmFyIHRleHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYWJvdXRfX2Rlc2NyaXB0aW9uJyk7XHJcblxyXG4gICAgICAgIHZhciBjdXJyZW50U2Nyb2xsID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG5cclxuICAgICAgICBpZiAoY3VycmVudFNjcm9sbCA+IGNvbnRlbnRDb29yZHMpIHtcclxuICAgICAgICAgICAgYmFubmVyLmNsYXNzTGlzdC5hZGQoJ2JvdW5jZUluTGVmdCcpO1xyXG4gICAgICAgICAgICBiYW5uZXIuY2xhc3NMaXN0LmFkZCgnYW5pbWF0ZWQnKTtcclxuICAgICAgICAgICAgdGV4dC5jbGFzc0xpc3QuYWRkKCdhbmltYXRlZCcpO1xyXG4gICAgICAgICAgICB0ZXh0LmNsYXNzTGlzdC5hZGQoJ2JvdW5jZUluUmlnaHQnKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY3VycmVudFNjcm9sbCA8IGNvbnRlbnRDb29yZHMpIHtcclxuICAgICAgICAgICAgYmFubmVyLmNsYXNzTGlzdC5yZW1vdmUoJ2FuaW1hdGVkJyk7XHJcbiAgICAgICAgICAgIGJhbm5lci5jbGFzc0xpc3QucmVtb3ZlKCdib3VuY2VJbkxlZnQnKTtcclxuICAgICAgICAgICAgdGV4dC5jbGFzc0xpc3QucmVtb3ZlKCdhbmltYXRlZCcpO1xyXG4gICAgICAgICAgICB0ZXh0LmNsYXNzTGlzdC5yZW1vdmUoJ2JvdW5jZUluUmlnaHQnKTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBzaG93Q29udGVudClcclxuXHJcbiAgICB9KTtcclxufSkoKTtcclxuXHJcblxyXG5cclxuIl0sImZpbGUiOiJhYm91dC5qcyJ9
