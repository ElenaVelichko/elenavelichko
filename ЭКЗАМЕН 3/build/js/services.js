;(() => {
    function showContent(event) {

        var content = document.querySelector('#services');

        var contentCoords = content.getBoundingClientRect().top + window.pageYOffset - content.offsetHeight;

        var currentScroll = window.pageYOffset;

        if (currentScroll > contentCoords) {
            content.classList.add('animated');
            content.classList.add('zoomIn');
        }

        if (currentScroll < contentCoords) {
            content.classList.remove('animated');
            content.classList.remove('zoomIn');
        }
    }
    document.addEventListener('DOMContentLoaded',() => {
        document.addEventListener('scroll', showContent)

    });
})();


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzZXJ2aWNlcy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyI7KCgpID0+IHtcclxuICAgIGZ1bmN0aW9uIHNob3dDb250ZW50KGV2ZW50KSB7XHJcblxyXG4gICAgICAgIHZhciBjb250ZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3NlcnZpY2VzJyk7XHJcblxyXG4gICAgICAgIHZhciBjb250ZW50Q29vcmRzID0gY29udGVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3AgKyB3aW5kb3cucGFnZVlPZmZzZXQgLSBjb250ZW50Lm9mZnNldEhlaWdodDtcclxuXHJcbiAgICAgICAgdmFyIGN1cnJlbnRTY3JvbGwgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblxyXG4gICAgICAgIGlmIChjdXJyZW50U2Nyb2xsID4gY29udGVudENvb3Jkcykge1xyXG4gICAgICAgICAgICBjb250ZW50LmNsYXNzTGlzdC5hZGQoJ2FuaW1hdGVkJyk7XHJcbiAgICAgICAgICAgIGNvbnRlbnQuY2xhc3NMaXN0LmFkZCgnem9vbUluJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY3VycmVudFNjcm9sbCA8IGNvbnRlbnRDb29yZHMpIHtcclxuICAgICAgICAgICAgY29udGVudC5jbGFzc0xpc3QucmVtb3ZlKCdhbmltYXRlZCcpO1xyXG4gICAgICAgICAgICBjb250ZW50LmNsYXNzTGlzdC5yZW1vdmUoJ3pvb21JbicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCBzaG93Q29udGVudClcclxuXHJcbiAgICB9KTtcclxufSkoKTtcclxuXHJcbiJdLCJmaWxlIjoic2VydmljZXMuanMifQ==
