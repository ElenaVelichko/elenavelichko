'use strict';

var gulp    = require('gulp'),
    wait    = require('gulp-wait'),
    concat  = require('gulp-concat'),
    // less    = require('gulp-less'),
    less    = require('gulp-less'),
    uglify  = require('gulp-uglify'),
    minify  = require('gulp-minify-css'),
    rename  = require('gulp-rename'),
    browserSync = require('browser-sync').create(),
    reload  = browserSync.reload,
    imagemin = require('gulp-imagemin'),
    pngquant = require('gulp-pngquant'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger  = require('gulp-rigger'),
 //   var del = require('del'),
    prefixer = require('gulp-autoprefixer');


var path = {
    build: {
        html:   'build/',
        js:     'build/js/',
        css:    'build/css/',
        img:    'build/img/',
        fonts:  'build/fonts'
    },
    src: {
        html:   'src/*.html',
        js:     'src/js/**/*.js',
        style:  'src/style/**/*.+(less|css)',
        img:    ['src/img/**/*.*', '!src/img/**/Thumbs.db'],
        fonts:  'src/fonts/**/*.*'
    },
    watch: {
        html:   'src/**/*.html',
        js:     'src/js/**/*.js',
        style:  'src/style/**/*.less',
        img:    'src/img/**/*.*',
        fonts:  'src/fonts/**/*.*'
    },
    clean: './build' //i've not make a task for this yet
};

var config = {
    server: {
        baseDir: "./build"
    },
    // tunnel: true,
    host:   'localhost',
    port: 7809,
    logPrefix: "beetrootAcademy"
};

// TASKS
gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true})); //|browserSync.stream()
});

gulp.task('js:build', function () {
    gulp.src('src/js/**/*.js')
//        .pipe(concat('main.js'))
        .pipe(sourcemaps.init())
//        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
 //       .pipe(wait(50)) //I catch an error w/ VSCode w/o this
 //       .pipe(sourcemaps.init())
        .pipe(less())
//           .on('error', less.logError) //when catching errors, gulp.watch should not crash
 //       .pipe(prefixer()) //add rules inside
 //       .pipe(minify())
 //       .pipe(sourcemaps.write())
//        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});



gulp.task('img:build', function() {
    gulp.src(path.src.img)
  //      .pipe(imagemin({
  //          progressive: true,
  //          svgoPlugins: [{removeViewBox: false}],
  //          use: [pngquant()],
  //          interlaced: true
  //      }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'img:build',
]);

gulp.task('watch', function() {
    gulp.watch([path.watch.html], ['html:build']);
    gulp.watch([path.watch.js], ['js:build']);
    gulp.watch([path.watch.style], ['style:build']);
    gulp.watch([path.watch.img], ['img:build']);
    gulp.watch([path.watch.fonts], ['fonts:build']);
});

gulp.task('webserver', function() {
    browserSync.init(config);
});

gulp.task('default', ['build', 'webserver', 'watch']);



/*
    browserSync = require('browser-sync'),

gulp.task('less', function(){
    return gulp.src('dev/less/**/ //  *.+(less|css)')
/*        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['IE > 9','last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('product/css'))
        .pipe(browserSync.stream());

});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'product/'
        },
        notify: false
    });
});

gulp.task('default', ['browser-sync'], function(){
    gulp.watch('dev/less/**/   //    *.less',['less']);
/*    gulp.watch('product/*.html', browserSync.reload)
});


*/
