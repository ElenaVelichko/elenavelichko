;(() => {
    function showContent(event) {

        var content = document.querySelector('#about');

        var contentCoords = content.getBoundingClientRect().top + window.pageYOffset - content.offsetHeight;

        var banner = document.querySelector('.about__banner');
        var text = document.querySelector('.about__description');

        var currentScroll = window.pageYOffset;

        if (currentScroll > contentCoords) {
            banner.classList.add('bounceInLeft');
            banner.classList.add('animated');
            text.classList.add('animated');
            text.classList.add('bounceInRight');

        }

        if (currentScroll < contentCoords) {
            banner.classList.remove('animated');
            banner.classList.remove('bounceInLeft');
            text.classList.remove('animated');
            text.classList.remove('bounceInRight');

        }
    }

    document.addEventListener('DOMContentLoaded',() => {
        document.addEventListener('scroll', showContent)

    });
})();



