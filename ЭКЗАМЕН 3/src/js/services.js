;(() => {
    function showContent(event) {

        var content = document.querySelector('#services');

        var contentCoords = content.getBoundingClientRect().top + window.pageYOffset - content.offsetHeight;

        var currentScroll = window.pageYOffset;

        if (currentScroll > contentCoords) {
            content.classList.add('animated');
            content.classList.add('zoomIn');
        }

        if (currentScroll < contentCoords) {
            content.classList.remove('animated');
            content.classList.remove('zoomIn');
        }
    }
    document.addEventListener('DOMContentLoaded',() => {
        document.addEventListener('scroll', showContent)

    });
})();

